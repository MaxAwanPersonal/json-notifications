# Max's Notification Stream

This app inputs a json file of the provided format and outputs notifications in the style of a social media site.

## Getting Started

* These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* linux (ideally a variant of ubuntu installed)
* mongodb
* python3
* pip3
* virtualenvwrapper


## Installing

### Start mongodb

* sudo service mongodb start


### Clone the repo and move into the directory

* git clone ...
* cd mongo_notifications

### Installing requirements

* mkvirtualenv --python=/usr/bin/python3 NOTIF
* pip install -r requirements.txt


### Loading the json file into MongoDB (only required on the first run)
* mongoimport --db notifications --collection notifications --file notifications-feed.json --jsonArray



### Running the app

* export LC_ALL=C.UTF-8
* export LANG=C.UTF-8
* workon NOTIF
* FLASK_APP=app.py flask run


## Running the tests

* workon NOTIF
* pytest


There are some basic test cases

One test case verifies that a correctly entered post id will return a page with notifcations.
    - As part of this, there is some basic testing to ensure that correct users are amongst the notifications and that incorrects user aren't.
The other verifies that an invalid post id will return a 404.


## Endpoints


* **URL**

  `/`

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `post_id=[alphanumeric]`


* **Success Response:**


  * **Code:** 200 <br />
    **Description:** `Upon clicking on the globe icon, you should see all relevant notifications to the entered post id`

* **Error Response:**


  * **Code:** 404 UNAUTHORIZED <br />
    **Description:** `Any invalid or incorrect post_id will return a 404`


* **Sample Call:**

* /b1638f970c3ddd528671df76c4dcf13e should will return the notifications associated with the post id b1638f970c3ddd528671df76c4dcf13e

## Deployment and Scaling

* For a simple solution, this could be deployed with elastic beanstalk using the following guide
* https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html

* This could also be done manually.
* For this, I'd create a docker container for the application and use something like AWS cloud formation to orchestrate.
Within this an application load balancer with an auto scaling group should cater for scaling.
I'd suggest the use of AWS cloudwatch for logging.


* Ideally, I would have created this as a serverless application purely with AWS:
* AWS API gateway to handle GET requests
* AWS Lambda to retrieve the relevant notifications from AWS DynamoDB and present them to the user

* A serverless approach would be a lot cheaper and would also scale easily without the need for orchestration.

## Built With

* [Python 3](https://docs.python.org/) - The language used
* [Flask](http://flask.pocoo.org/docs) - The web framework used
* [MongoDB](https://docs.mongodb.com) - The NoSQL database used



## Authors

* **Max Awan** *


## Acknowledgments

* Hat tip to anyone whose code was used
