import unittest
from app import app
import pytest


class TestPostEndpoint(unittest.TestCase):

    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def test_valid_post(self):
        response = self.app.get("/b1638f970c3ddd528671df76c4dcf13e")
        self.assertEqual(response.status_code, 200)
        # Assert post title is in html
        self.assertIn("Acme Inc dynamically scales niches worldwide", str(response.data))
        # Assert that a user who liked and commented on the post shows up
        self.assertIn("Suoma Narjus", str(response.data))
        # Assert that a user who liked the post shows up
        self.assertIn("Mary T. Price", str(response.data))
        # Assert that a user who did not like nor comment on the post does not show up
        self.assertNotIn("Chuck Looij", str(response.data))

    def test_comment_user_shows_on_valid_post(self):
        # This could not be tested with the same post id as there was no distinct user who solely commented on the post
        response = self.app.get("/57e0d6328c9287bd1b66bc327efbcdfa")
        self.assertIn("Chuck Looij", str(response.data))

    def test_invalid_post(self):
        response = self.app.get("/h3llo")
        self.assertEqual(response.status_code, 404)

    # def test_unsupported_methods_fail(self):
    #
    #     for method in ["put", "post", "patch", "head", "delete"]:
    #         client_method = getattr(self.app, method)
    #         response = client_method(
    #             "b1638f970c3ddd528671df76c4dcf13e",
    #             {})
    #         self.assertEqual(response.status_code, 405)
