from flask import Flask, render_template, abort
from db import db
notifications = db.notifications

app = Flask(__name__, static_url_path='/static')

@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500

@app.route('/<post_id>', methods=['GET'])
def notifications(post_id=None):

    first_notification = db.notifications.find_one({"post.id": post_id}, {"post.title": 1})
    if first_notification:
        post_title = first_notification["post"]["title"]
    else:
        abort(404)
    likes = db.notifications.find({"post.id": post_id, "type": "Like"}, {"user.name": 1})
    comments = db.notifications.find({"post.id": post_id, "type": "Comment"}, {"user.name": 1})
    like_users = []
    comment_users = []
    for doc in likes:
        like_users.append(doc["user"]["name"])
    for doc in comments:
        comment_users.append(doc["user"]["name"])
    total_count = len(like_users) + len(comment_users)
    return render_template('notification_feed.html', like_users=like_users,
                           comment_users=comment_users, post_title=post_title, total_count=total_count)



